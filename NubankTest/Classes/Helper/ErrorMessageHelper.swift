//
//  ErrorMessageHelper.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class ErrorMessageHelper: UIView {

    private static var errorToken : dispatch_once_t = 0
    
    private static var errorView : ViewErrorMessage?

    static func showErrorMessage(errorMessage: String?) {
        dispatch_once(&errorToken) {
            self.errorView = ViewErrorMessage.newInstance()
            self.errorView?.frame = UIScreen.mainScreen().bounds
            self.errorView?.btnClose.addTarget(self, action: "dissmissErrorMessage", forControlEvents: .TouchUpInside)
        }
        
        self.errorView?.lblMessage.text = errorMessage
        
        if let window : UIWindow = UIApplication.sharedApplication().keyWindow {
            window.addSubview(self.errorView!)
        }
    }
    
    static func dissmissErrorMessage() {
        self.errorView?.removeFromSuperview()
    }
    
}
