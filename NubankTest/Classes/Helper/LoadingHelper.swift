//
//  LoadingHelper.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class LoadingHelper: NSObject {

    private static var loadingToken: dispatch_once_t = 0
    
    private static var loadingView : UIView?
    
    static func showLoading() {
        dispatch_once(&loadingToken) {
            self.loadingView = NSBundle(forClass: self).loadNibNamed("LoadingView", owner: self, options: nil)[0] as? UIView
            self.loadingView?.frame = UIScreen.mainScreen().bounds
        }
        
        if let window : UIWindow = UIApplication.sharedApplication().keyWindow {
            window.addSubview(self.loadingView!)
        }
    }
    
    static func dissmissLoading() {
        self.loadingView?.removeFromSuperview()
    }
}
