//
//  SaveContestRequest.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class SaveContestRequest: JSONModel {

    var comment: String?
    var listReasonDetails = Array<ReasonDetail>()
    
    // JSONModel was not able to create the right dictionary, so I do it mannually
    func toJsonObject() -> [String : AnyObject]! {
        var listDic = Array<NSDictionary>()
        
        for reasonDetail in self.listReasonDetails {
            listDic.append(reasonDetail.toDictionary())
        }
        
        let jsonObject : [String : AnyObject]! = [
            "comment" : self.comment!,
            "reason_details": listDic
        ]
        
        return jsonObject
    }
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
}
