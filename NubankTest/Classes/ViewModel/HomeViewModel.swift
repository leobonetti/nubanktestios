//
//  HomeViewModel.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import Alamofire
import SwiftTryCatch

class HomeViewModel: NSObject {

    func getNoticeEndPoint(vcHome: VCHome) {
        if ConnectivityHelper.isOnline() {
            self.callNoticeEndPointService(vcHome)
        }
        else {
            vcHome.onError(Constants.Error.ErrorDefaultNoConnection)
        }
    }
    
    private func callNoticeEndPointService(vcHome: VCHome) {
        LoadingHelper.showLoading()
        
        Alamofire.request(.GET, Constants.URL.StaticURL)
            .responseString { response in
                if response.result.isSuccess {
                    var link: Link?
                    
                    SwiftTryCatch.tryBlock({
                        
                        link = Link(string: response.result.value, error: nil)
                        
                    }, catchBlock: { error in
                        
                        link = nil
                        
                    }, finallyBlock: {
                        
                        self.analyzeNoticeEndPointResponse(vcHome, link: link)
                        
                    })
                }
                else {
                    LoadingHelper.dissmissLoading()
                    vcHome.onError(response.result.error?.localizedDescription)
                }
        }
    }
    
    private func analyzeNoticeEndPointResponse(vcHome: VCHome, link: Link?) {
        if link == nil || link?.tryLinkConversion() == false || link?.links?[Constants.EndPoints.Notice] == nil {
            LoadingHelper.dissmissLoading()
            vcHome.onError(Constants.Error.ErrorIncorrectAnswer)
            
            return
        }
        
        self.getNoticeData(vcHome, endPoint: (link?.links?[Constants.EndPoints.Notice])!)
    }
    
    private func getNoticeData(vcHome: VCHome, endPoint: EndPoint) {
        if ConnectivityHelper.isOnline() {
            self.callNoticeDataService(vcHome, endPoint: endPoint)
        }
        else {
            vcHome.onError(Constants.Error.ErrorDefaultNoConnection)
        }
    }
    
    private func callNoticeDataService(vcHome: VCHome, endPoint: EndPoint) {
        Alamofire.request(.GET, endPoint.href!)
            .responseString { response in
                if response.result.isSuccess {                    
                    var noticeData: NoticeData?
                    
                    SwiftTryCatch.tryBlock({
                        
                        noticeData = NoticeData(string: response.result.value, error: nil)
                        
                    }, catchBlock: { error in
                            
                        noticeData = nil
                            
                    }, finallyBlock: {
                            
                        self.analyzeNoticeDataServiceResponse(vcHome, noticeData: noticeData)
                            
                    })
                    
                }
                else {
                    vcHome.onError(response.result.error?.localizedDescription)
                }
                
                LoadingHelper.dissmissLoading()
        }

    }
    
    private func analyzeNoticeDataServiceResponse(vcHome: VCHome, noticeData: NoticeData?) {
        if noticeData == nil || noticeData?.isDomainInvalid() == true {
            vcHome.onError(Constants.Error.ErrorIncorrectAnswer)
            
            return
        }
        
        self.goToNoticeView(vcHome, noticeData: noticeData!)
    }
    
    private func goToNoticeView(vcHome: VCHome, noticeData: NoticeData) {
        vcHome.navigationController?.pushViewController(VCNotice(noticeData: noticeData), animated: false)
    }
}









 