
//
//  NoticeViewModel.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import Alamofire
import SwiftTryCatch

class NoticeViewModel: NSObject {

    func handleAction(vcNotice: VCNotice, action: Action, noticeData: NoticeData) {
        if action.action == Constants.Action.Continue {
            self.getChargebackData(vcNotice, endPoint: (noticeData.links?[Constants.EndPoints.Chargeback])!)
            return
        }
        
        if action.action == Constants.Action.Cancel {
            vcNotice.navigationController?.popViewControllerAnimated(false)
            return
        }
        
        vcNotice.navigationController?.popViewControllerAnimated(false)
    }
    
    private func getChargebackData(vcNotice: VCNotice, endPoint: EndPoint) {
        if ConnectivityHelper.isOnline() {
            self.callChargebackDataServiceCall(vcNotice, endPoint: endPoint)
        }
        else {
            vcNotice.onError(Constants.Error.ErrorDefaultNoConnection)
        }
    } 
    
    private func callChargebackDataServiceCall(vcNotice: VCNotice, endPoint: EndPoint) {
        LoadingHelper.showLoading()
        
        Alamofire.request(.GET, endPoint.href!)
            .responseString { response in
                if response.result.isSuccess {                    
                    var chargebackData: ChargebackData?
                    
                    SwiftTryCatch.tryBlock({
                        
                        chargebackData = ChargebackData(string: response.result.value, error: nil)
                        
                    }, catchBlock: { error in
                            
                        chargebackData = nil
                            
                    }, finallyBlock: {
                            
                        self.analyzeChargebackDataServiceResponse(vcNotice, chargebackData: chargebackData)
                            
                    })
                }
                else {
                    vcNotice.onError(response.result.error?.localizedDescription)
                }
                
                LoadingHelper.dissmissLoading()
        }
    }
    
    private func analyzeChargebackDataServiceResponse(vcNotice: VCNotice, chargebackData: ChargebackData?) {
        if chargebackData == nil || chargebackData?.isDomainInvalid() == true {
            vcNotice.onError(Constants.Error.ErrorIncorrectAnswer)
            
            return
        }
        
        self.goToChargeBackView(vcNotice, chargebackData: chargebackData!)
    }
    
    private func goToChargeBackView(vcNotice: VCNotice, chargebackData: ChargebackData) {
        vcNotice.navigationController?.pushViewController(VCChargeback(chargebackData: chargebackData), animated: false)
    }
}







