//
//  ChargebackViewModel.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import Alamofire
import SwiftTryCatch

class ChargebackViewModel: NSObject {

    func blockCardIfNecessary(vcChargeback: VCChargeback, chargebackData: ChargebackData) {
        if(Bool(chargebackData.autoblock!)) {
            self.blockCard(vcChargeback, chargebackData: chargebackData)
        }
    }
    
    func blockCard(vcChargeback: VCChargeback, chargebackData: ChargebackData) {
        if ConnectivityHelper.isOnline() {
            self.callBlockCardService(vcChargeback, endPoint: (chargebackData.links?[Constants.EndPoints.BlockCard])!)
        }
        else {
            vcChargeback.onError(Constants.Error.ErrorDefaultNoConnection)
        }
    }
    
    private func callBlockCardService(vcChargeback: VCChargeback, endPoint: EndPoint) {
        LoadingHelper.showLoading()
        
        Alamofire.request(.POST, endPoint.href!)
            .responseString { response in
                if response.result.isSuccess {
                    var status: Status?
                    
                    SwiftTryCatch.tryBlock({
                        
                        status = Status(string: response.result.value, error: nil)
                        
                    }, catchBlock: { error in
                        
                        status = nil
                        
                    }, finallyBlock: {
                        
                        self.analyzeBlockCardServiceResponse(vcChargeback, status: status)
                            
                    })
                }
                else {
                    vcChargeback.onError(response.result.error?.localizedDescription)
                }
                
                LoadingHelper.dissmissLoading()
        }
    }
    
    private func analyzeBlockCardServiceResponse(vcChargeback: VCChargeback, status: Status?) {
        if status == nil || status?.isDomainInvalid() == true || status?.status! != Constants.Status.Ok {
            vcChargeback.onError(Constants.Error.ErrorIncorrectAnswer)
            
            return
        }
        
        vcChargeback.onBlockCardSuccess()
    }
    
    func unblockCard(vcChargeback: VCChargeback, chargebackData: ChargebackData) {
        if ConnectivityHelper.isOnline() {
            self.callUnblockCardService(vcChargeback, endPoint: (chargebackData.links?[Constants.EndPoints.UnblockCard])!)
        }
        else {
            vcChargeback.onError(Constants.Error.ErrorDefaultNoConnection)
        }
    }
    
    private func callUnblockCardService(vcChargeback: VCChargeback, endPoint: EndPoint) {
        LoadingHelper.showLoading()
        
        Alamofire.request(.POST, endPoint.href!)
            .responseString { response in
                if response.result.isSuccess {
                    var status: Status?
                    
                    SwiftTryCatch.tryBlock({
                        
                        status = Status(string: response.result.value, error: nil)
                        
                    }, catchBlock: { error in
                        
                        status = nil
                        
                    }, finallyBlock: {
                        
                        self.analyzeUnblockCardServiceResponse(vcChargeback, status: status)
                            
                    })
                }
                else {
                    vcChargeback.onError(response.result.error?.localizedDescription)
                }
                
                LoadingHelper.dissmissLoading()
        }
    }
    
    private func analyzeUnblockCardServiceResponse(vcChargeback: VCChargeback, status: Status?) {
        if status == nil || status?.isDomainInvalid() == true || status?.status! != Constants.Status.Ok {
            vcChargeback.onError(Constants.Error.ErrorIncorrectAnswer)
            
            return
        }
        
        vcChargeback.onUnblockCardSuccess()
    }
    
    func saveConstest(vcChargeback: VCChargeback, chargebackData: ChargebackData, comment: String?) {
        if ConnectivityHelper.isOnline() {
            let saveContestRequest = SaveContestRequest()
            saveContestRequest.comment = comment
            saveContestRequest.listReasonDetails = chargebackData.listReasonDetails!
            
            self.callSaveContestService(vcChargeback, endPoint: (chargebackData.links?[Constants.EndPoints.ChargebackSelf])!, saveContestRequest: saveContestRequest)
        }
        else {
            vcChargeback.onError(Constants.Error.ErrorDefaultNoConnection)
        }
    }
    
    private func callSaveContestService(vcChargeback: VCChargeback, endPoint: EndPoint, saveContestRequest: SaveContestRequest) {
        LoadingHelper.showLoading()
        
        Alamofire.request(.POST, endPoint.href!, parameters: saveContestRequest.toJsonObject(), encoding: .JSON)
            .responseString { response in
                if response.result.isSuccess {
                    var status: Status?
                    
                    SwiftTryCatch.tryBlock({
                        
                        status = Status(string: response.result.value, error: nil)
                        
                    }, catchBlock: { error in
                        
                        status = nil
                        
                    }, finallyBlock: {
                        
                        self.analyzeSaveContestServiceResponse(vcChargeback, status: status)
                            
                    })
                }
                else {
                    vcChargeback.onError(response.result.error?.localizedDescription)
                }
                
                LoadingHelper.dissmissLoading()
        }
    }
    
    private func analyzeSaveContestServiceResponse(vcChargeback: VCChargeback, status: Status?) {
        if status == nil || status?.isDomainInvalid() == true || status?.status! != Constants.Status.Ok {
            vcChargeback.onError(Constants.Error.ErrorIncorrectAnswer)
            
            return
        }
        
        self.goToChargebackSuccessView(vcChargeback)
    }
    
    private func goToChargebackSuccessView(vcChargeback: VCChargeback) {
        vcChargeback.navigationController?.pushViewController(VCChargebackSuccess(), animated: false)
    }
    
    func onCancel(vcChargeback: VCChargeback) {
        vcChargeback.navigationController?.popViewControllerAnimated(false)
    }
}












