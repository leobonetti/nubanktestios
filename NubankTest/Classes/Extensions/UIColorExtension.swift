//
//  UIColorExtension.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/16/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

extension UIColor {
 
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red) / 255
        let newGreen = CGFloat(green) / 255
        let newBlue = CGFloat(blue) / 255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1)
    }
    
    public class func disabledGrayColor() -> UIColor {
        return UIColor(red: 204, green: 204, blue: 204)
    }
    
    public class func defaultBlackColor() -> UIColor {
        return UIColor(red: 34, green: 34, blue: 34)
    }
    
    public class func backgroundColor() -> UIColor {
        return UIColor(red: 253, green: 253, blue: 253)
    }
    
    public class func defaultGreenColor() -> UIColor {
        return UIColor(red: 65, green: 117, blue: 5)
    }
    
    public class func hintGrayColor() -> UIColor {
        return UIColor(red: 153, green: 153, blue: 153)
    }
}