//
//  UIViewExtension.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

extension UIView {
    
    func updateFrame(x: CGFloat?, y: CGFloat?, width: CGFloat?, height: CGFloat?) {
        var frame = self.frame
        
        if let newX = x {
            frame.origin.x = newX
        }
        
        if let newY = y {
            frame.origin.y = newY
        }
        
        if let newWidth = width {
            frame.size.width = newWidth
        }
        
        if let newHeight = height {
            frame.size.height = newHeight
        }
        
        self.frame = frame
    }
    
}
