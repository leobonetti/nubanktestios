//
//  Constants.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

struct Constants {
    struct URL {
        static let StaticURL = "https://nu-mobile-hiring.herokuapp.com"
    }
    
    struct Error {
        static let ErrorDefaultNoConnection = "A sua solicitação falhou. Por favor, verifique sua conexão com a Internet"
        static let ErrorIncorrectAnswer = "Estamos tendo problemas internos no momento. Por favor, tente novamente em breve. Caso o erro persista, entre em contato conosco"
    }
    
    struct EndPoints {
        static let Notice = "notice"
        static let Chargeback = "chargeback"
        static let BlockCard = "block_card"
        static let UnblockCard = "unblock_card"
        static let ChargebackSelf = "self"
    }
    
    struct Action {
        static let Continue = "continue"
        static let Cancel = "cancel"
    }
    
    struct Status {
        static let Ok = "Ok"
    }
}
