//
//  ViewReasonDetail.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/16/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class ViewChargebackReasonDetail: UIView {
   
    @IBOutlet weak var lblTitle: UILabelDefault!
    @IBOutlet weak var swReason: UISwitch!

    class func newInstance() -> ViewChargebackReasonDetail {
        return NSBundle(forClass: self).loadNibNamed("ChargebackReasonDetailView", owner: self, options: nil)[0] as! ViewChargebackReasonDetail
    }
}
