//
//  ViewErrorMessage.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class ViewErrorMessage: UIView {

    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var btnClose: UIButton!

    class func newInstance() -> ViewErrorMessage {
        return NSBundle(forClass: self).loadNibNamed("ErrorMessageView", owner: self, options: nil)[0] as! ViewErrorMessage
    }
    
}
