//
//  ReasonDetail.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class ReasonDetail: JSONModel {

    var id: String?
    var title: String?
    var response = false
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }

    func isDomaindInvalid() -> Bool {
        return self.id == nil || self.title == nil
    }
    
}
