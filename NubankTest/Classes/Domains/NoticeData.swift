    //
//  NoticeData.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class NoticeData: Link {

    var title: String?
    
    // Cant be called description because conflicts with Objective-C
    var descr: String?
    var primaryAction: Action?
    var secondaryAction: Action?
    
    override class func keyMapper() -> JSONKeyMapper! {
        return JSONKeyMapper(dictionary:
            [
                "links" : "responseLinks",
                "description" : "descr",
                "primary_action" : "primaryAction",
                "secondary_action" : "secondaryAction"
            ])
    }
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
    private func isLinksInvalid() -> Bool {
        return self.links?[Constants.EndPoints.Chargeback] == nil
    }
    
    func isDomainInvalid() -> Bool {
        return self.title == nil || self.descr == nil || self.primaryAction == nil || self.secondaryAction == nil || self.tryLinkConversion() == false || self.isLinksInvalid()
    }
    
}
