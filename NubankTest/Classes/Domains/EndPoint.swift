//
//  EndPoint.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class EndPoint: JSONModel {

    var href : String?
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
}
