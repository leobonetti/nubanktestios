//
//  Link.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class Link: JSONModel {

    // JSONModel is not able to convert a dictionary inside a dictionary to its corresponding object, so i'll use this var to retreive data from the webservice
    var responseLinks = [String : NSDictionary]()
    
    // And use this one on the app, to avoid creating a DTO to all response data just because of it
    var links : [String : EndPoint]?
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
    override class func keyMapper() -> JSONKeyMapper! {
        return JSONKeyMapper(dictionary: ["links" : "responseLinks"])
    }
    
    override class func propertyIsIgnored(propertyName: String!) -> Bool {
        if propertyName == "links" {
            return true
        }
        
        return false
    }
    
    // Return true for a successfull convertion
    func tryLinkConversion() -> Bool {
        self.links = [String : EndPoint]()
        
        for (key, value) in self.responseLinks {            
            do {
                let endPoint = try EndPoint(dictionary: value as [NSObject : AnyObject])
                self.links?[key] = endPoint
            }
            catch {
                return false
            }
        }
        
        return true
    }
}






