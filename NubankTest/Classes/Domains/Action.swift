//
//  Action.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/19/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class Action: JSONModel {

    var title: String?
    var action: String?
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
}
