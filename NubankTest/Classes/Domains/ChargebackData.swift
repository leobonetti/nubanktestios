//
//  ChargebackData.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class ChargebackData: Link {
    
    var commentHint: String?
    var id: String?
    var title: String?
    
    // JSONModel is not able to converte a Bool type, so using NSNumber in place
    var autoblock: NSNumber?
    
    // JSONModel is not able to convert custom object inside Arrays
    var listReasonDetailsResponse = Array<NSDictionary>()
    
    var listReasonDetails: Array<ReasonDetail>?

    override class func keyMapper() -> JSONKeyMapper! {
        return JSONKeyMapper(dictionary:
            [
                "links" : "responseLinks",
                "comment_hint" : "commentHint",
                "reason_details" : "listReasonDetailsResponse"
            ])
    }
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
    private func isLinksInvalid() -> Bool {
        return self.links?[Constants.EndPoints.BlockCard] == nil ||
                self.links?[Constants.EndPoints.UnblockCard] == nil ||
                self.links?[Constants.EndPoints.ChargebackSelf] == nil
    }
    
    // Return true for a successfull convertion
    private func tryReasonDetailsConversion() -> Bool {
        self.listReasonDetails = Array<ReasonDetail>()
        
        for reasonDetailDictionary in self.listReasonDetailsResponse {
            do {
                let reasonDetail = try ReasonDetail(dictionary: reasonDetailDictionary as [NSObject : AnyObject])
                if reasonDetail.isDomaindInvalid() {
                    return false
                }
                
                self.listReasonDetails?.append(reasonDetail)
            }
            catch {
                return false
            }
        }
        
        return true
    }
    
    func isDomainInvalid() -> Bool {
        return self.commentHint == nil ||
                self.id == nil ||
                self.title == nil ||
                self.autoblock == nil ||
                self.tryReasonDetailsConversion() == false ||
                self.tryLinkConversion() == false ||
                self.isLinksInvalid()
    }
}
