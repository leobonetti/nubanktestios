//
//  Status.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/20/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit
import JSONModel

class Status: JSONModel {

    var status: String?
    
    override class func propertyIsOptional(propertyName: String!) -> Bool {
        return true
    }
    
    func isDomainInvalid() -> Bool {
        return self.status == nil
    }
    
}
