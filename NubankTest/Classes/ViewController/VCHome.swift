//
//  VCHome.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/14/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class VCHome: UIViewController {
    
    private var viewModel : HomeViewModel?
    
    // MARK: Initializers
    
    init() {
        super.init(nibName: "HomeView", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewModel = HomeViewModel()
    }
    
    override func viewWillAppear(animated: Bool) {
        self.viewModel?.getNoticeEndPoint(self)
    }
    
    // MARK: Internal methods
    
    func onError(errorMessage: String?) {
        ErrorMessageHelper.showErrorMessage(errorMessage)
    }
    
    // MARK: Interface interaction
    
    @IBAction func onContestBuyTapped() {
        self.viewModel?.getNoticeEndPoint(self)
    }
    
    // MARK: Memory management

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
