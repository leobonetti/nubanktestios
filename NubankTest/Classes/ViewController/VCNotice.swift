//
//  VCNotice.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/14/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class VCNotice: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tvDescription: UITextView!
    
    @IBOutlet weak var btnPrimaryAction: UIButton!
    @IBOutlet weak var btnSecondaryAction: UIButton!
    
    var noticeData: NoticeData?
    var viewModel: NoticeViewModel?
    
    // MARK: Initializers
    
    init(noticeData: NoticeData) {
        super.init(nibName: "NoticeView", bundle: nil)
        
        self.noticeData = noticeData
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.viewModel = NoticeViewModel()
        self.initUI()
    }
    
    // MARK: Internal methods
    
    func initUI() {
        self.lblTitle.text = self.noticeData?.title
        self.tvDescription.attributedText = self.noticeData?.descr!.toHtml()
        self.btnPrimaryAction.setTitle(self.noticeData?.primaryAction!.title, forState: .Normal)
        self.btnSecondaryAction.setTitle(self.noticeData?.secondaryAction!.title, forState: .Normal)
    }
    
    func onError(errorMessage: String?) {
        ErrorMessageHelper.showErrorMessage(errorMessage)
    }

    // MARK: Interface interaction
    
    @IBAction func onContinueTapped() {
        self.viewModel?.handleAction(self, action: self.noticeData!.primaryAction!, noticeData: self.noticeData!)
    }
    
    @IBAction func onCancelTapped() {
        self.viewModel?.handleAction(self, action: self.noticeData!.secondaryAction!, noticeData: self.noticeData!)
    }
    
    // MARK: Memory management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
