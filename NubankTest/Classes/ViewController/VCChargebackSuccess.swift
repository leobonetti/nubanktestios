//
//  VCChargebackSuccess.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/15/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class VCChargebackSuccess: UIViewController {

    // MARK: Initializers
    
    init() {
        super.init(nibName: "ChargebackSuccessView", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
    }

    // MARK: Interface interaction
    
    @IBAction func onCloseTapped() {
        self.navigationController?.popToRootViewControllerAnimated(false)
    }
    
    // MARK: Memory management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
