//
//  VCChargeback.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/15/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

class VCChargeback: UIViewController, UITextViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewBackground: UIViewBackground!
    @IBOutlet var viewContainer: UIView!
    @IBOutlet weak var viewReasonDetail: UIView!
    @IBOutlet weak var viewLockedCard: UIView!
    @IBOutlet weak var viewUnlockedCard: UIView!
    @IBOutlet weak var viewDetails: UIView!
    
    // There is no placeholder for UITextViews, so this view is a little hack to accomplish it
    @IBOutlet weak var tvPlaceholder: UITextView!
    @IBOutlet weak var tvDetails: UITextView!
    
    @IBOutlet weak var btnContest: UIButtonEnabled!    
    
    var listReasonDetailView : Array<ViewChargebackReasonDetail>?
    
    var keyboardHeight : CGFloat = 0.0
    
    var chargebackData: ChargebackData?
    var viewModel: ChargebackViewModel?
    
    // MARK: Initializers
    
    init(chargebackData: ChargebackData) {
        super.init(nibName: "ChargebackView", bundle: nil)
        
        self.chargebackData = chargebackData
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initUI()
        
        self.viewModel = ChargebackViewModel()
        self.viewModel?.blockCard(self, chargebackData: self.chargebackData!)
        
        self.btnContest.customEnabled(false)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillShowNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.viewContainer.updateFrame(nil, y: 0.0, width: self.scrollView.bounds.width, height: nil)
        
        self.setUpReasonDetailsViews()
        self.setUpDetailsViewPosition()
        self.setUpContainerView()
        self.setUpScrollView()
    }

    // MARK: Internal methods
    
    func initUI() {
        self.lblTitle.text = self.chargebackData?.title
        
        // For some reason, setting an attributedText resets the text color
        self.tvPlaceholder.attributedText = self.chargebackData?.commentHint?.toHtml()        
        self.tvPlaceholder.textColor = UIColor.hintGrayColor()
    }
    
    func setUpReasonDetailsViews() {
        if (self.listReasonDetailView == nil) {
            self.listReasonDetailView = Array()
        }
        
        let originalHeight = self.viewReasonDetail.bounds.height
        
        let listSize = self.chargebackData?.listReasonDetails?.count
        for var i = 0; i < listSize; i++ {
            let view = ViewChargebackReasonDetail.newInstance()
            
            view.updateFrame(nil, y: originalHeight + view.bounds.height * CGFloat(i), width: self.scrollView.bounds.width, height: nil)
            self.viewReasonDetail.updateFrame(nil, y: nil, width: nil, height: self.viewReasonDetail.bounds.height + view.bounds.height)
            
            self.viewReasonDetail.addSubview(view)
            
            let reasonDetails = self.chargebackData?.listReasonDetails?[i]
            view.lblTitle.text = reasonDetails?.title
            view.swReason.addTarget(self, action: "onSwitchChange:", forControlEvents: UIControlEvents.ValueChanged)
            view.swReason.tag = i
            
            self.listReasonDetailView?.append(view)
        }
    }
    
    func setUpDetailsViewPosition() {
        self.viewDetails.updateFrame(nil, y: self.viewReasonDetail.bounds.height, width: nil, height: nil)
    }
    
    func setUpContainerView() {
        self.viewContainer.updateFrame(nil, y: nil, width: nil, height: self.viewReasonDetail.bounds.height + self.viewDetails.bounds.height)
    }
    
    func setUpScrollView() {
        self.scrollView.autoresizesSubviews = false
        self.scrollView.contentSize = self.viewContainer.frame.size
        self.scrollView.addSubview(self.viewContainer)
    }
    
    func onBlockCardSuccess() {
        self.viewLockedCard.hidden = false
        self.viewUnlockedCard.hidden = true
    }

    func onUnblockCardSuccess() {
        self.viewLockedCard.hidden = true
        self.viewUnlockedCard.hidden = false
    }
    
    func onError(errorMessage: String?) {
        ErrorMessageHelper.showErrorMessage(errorMessage)
    }
    
    // MARK: Interface interaction
    
    func onSwitchChange(selectedSwitch: UISwitch) {
        if let view = self.listReasonDetailView?[selectedSwitch.tag] {
            var textColor = UIColor.defaultBlackColor()
            if selectedSwitch.on {
                textColor = UIColor.defaultGreenColor()
            }            
            view.lblTitle.textColor = textColor
            
            if let reasonDetails = self.chargebackData?.listReasonDetails?[selectedSwitch.tag] {
                reasonDetails.response = selectedSwitch.on
            }
        }
    }
    
    @IBAction func onLockedCardTapped() {
        self.viewModel?.unblockCard(self, chargebackData: self.chargebackData!)
    }
    
    @IBAction func onUnlockedCardTapped() {
        self.viewModel?.blockCard(self, chargebackData: self.chargebackData!)
    }
    
    @IBAction func onCancelTapped() {
        self.view.endEditing(true)
        self.viewModel?.onCancel(self)
    }
    
    @IBAction func onContestTapped() {
        self.view.endEditing(true)
        self.viewModel?.saveConstest(self, chargebackData: self.chargebackData!, comment: self.tvDetails.text)
    }
    
    // MARK: Keyboard
    
    func keyboardWillShow(notification:NSNotification) {
        let userInfo:NSDictionary = notification.userInfo!
        let keyboardFrame:NSValue = userInfo.valueForKey(UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.CGRectValue()
        self.keyboardHeight = keyboardRectangle.height
    }
    
    // MARK: TextViewDelegate
    
    // Used to fake the placeholder
    func textViewDidChange(textView: UITextView) {
        self.tvPlaceholder.hidden = !textView.text.isEmpty
        self.btnContest.customEnabled(!textView.text.isEmpty)
    }
    
    // Used to control the view position while texting
    func textViewDidBeginEditing(textView: UITextView) {
        let textViewFrameOnViewCoordinates = textView.convertRect(textView.frame, toView: self.view)
        
        if ( textViewFrameOnViewCoordinates.origin.y + textView.frame.size.height ) >
            ( self.view.frame.size.height - self.keyboardHeight ) {
            let newY = self.viewBackground.frame.origin.y - ( (textViewFrameOnViewCoordinates.origin.y + textView.frame.size.height) - (self.view.frame.size.height - self.keyboardHeight) + 5.0 )
            self.viewBackground.updateFrame(nil, y: newY, width: nil, height: nil)
        }
    }
    
    // Used to return the view to it's initial state
    func textViewShouldEndEditing(textView: UITextView) -> Bool {
        self.viewBackground.updateFrame(nil, y: 20.0, width: nil, height: nil)
        
        return true
    }
    
    // Used to fake a Done button on a textview
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            self.view.endEditing(true)
            return false
        }
        
        return true
    }
    
    // MARK: Memory management
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }    

}
