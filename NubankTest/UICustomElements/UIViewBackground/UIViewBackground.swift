//
//  UIViewBackground.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/16/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

@IBDesignable
class UIViewBackground: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.load()
    }
    
    override func prepareForInterfaceBuilder() {
        self.load()
    }
    
    func load() {
        self.backgroundColor = UIColor.backgroundColor()
        self.layer.cornerRadius = 5.0
    }

}
