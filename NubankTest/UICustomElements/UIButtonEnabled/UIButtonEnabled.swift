//
//  UIButtonEnabled.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/15/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

@IBDesignable
class UIButtonEnabled: UIButton {
    
    @IBOutlet var btnCustom: UIButton!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.load()
    }
    
    override func prepareForInterfaceBuilder() {
        self.load()
    }

    func load() {
        self.btnCustom = NSBundle(forClass: self.dynamicType).loadNibNamed("UIButtonEnabled", owner: self, options: nil)[0] as! UIButton
        self.btnCustom.frame = self.bounds
        self.btnCustom.setTitle(self.titleLabel?.text, forState: .Normal)
        self.titleLabel?.layer.opacity = 0.0
        
        self.addSubview(self.btnCustom)
    }
    
    @IBAction func touchUpInsideEvent() {
        self.sendActionsForControlEvents(.TouchUpInside)
    }
    
    func customEnabled(enabled: Bool) {
        self.enabled = enabled
        self.btnCustom.enabled = enabled
        
        var fontSize = 16
        if enabled {
            fontSize = 18
        }
        
        self.btnCustom.titleLabel?.font = UIFont(name: (self.btnCustom.titleLabel?.font?.fontName)!, size: CGFloat(fontSize))
    }
}
