//
//  UIButtonCloseBorderTop.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/16/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

@IBDesignable
class UIButtonCloseBorderTop: UIButtonClose {

    override func load() {
        super.load()
        
        let line = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: 1.0))
        line.backgroundColor = UIColor.disabledGrayColor()
        line.autoresizingMask = UIViewAutoresizing.FlexibleWidth
        self.addSubview(line)
    }
    
}
