//
//  UIButtonClose.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/16/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

@IBDesignable
class UIButtonClose: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.load()
    }
    
    override func prepareForInterfaceBuilder() {
        self.load()
    }
    
    func load() {
        let button = NSBundle(forClass: self.dynamicType).loadNibNamed("UIButtonClose", owner: self, options: nil)[0] as! UIButton
        button.frame = self.bounds
        button.setTitle(self.titleLabel?.text, forState: .Normal)
        self.titleLabel?.layer.opacity = 0.0
        
        self.addSubview(button)
    }
    
    @IBAction func touchUpInsideEvent() {
        self.sendActionsForControlEvents(.TouchUpInside)
    }

    
}
