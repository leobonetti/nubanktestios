//
//  UILabelTitle.swift
//  NubankTest
//
//  Created by Leonardo Bonetti on 3/16/16.
//  Copyright © 2016 Leonardo Bonetti. All rights reserved.
//

import UIKit

@IBDesignable
class UILabelRed: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.load()
    }
    
    override func prepareForInterfaceBuilder() {
        self.load()
    }
    
    func load() {
        let label = NSBundle(forClass: self.dynamicType).loadNibNamed("UILabelRed", owner: self, options: nil)[0] as! UILabel
        label.frame = self.bounds
        label.text = self.text
        self.text = ""
        
        self.addSubview(label)
    }


}
