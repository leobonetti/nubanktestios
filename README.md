iOS app for Nubank mobile hiring test.

To set up the project: 

  1. After cloning, run "pod install";
  2. For a better experience with the .xib files, enable refresh views: Editor -> Automatically Refresh Views. This way, all .xib files will compile the UICustomElements created;